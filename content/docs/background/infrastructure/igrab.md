---
title: "igrab.py kernel maintainer tool"
linktitle: "igrab"
description: General information regarding third-party tooling.
weight: 70
---
## What is igrab?

*I*'m gonna go *grab* some testcase data == *igrab*

[igrab] is a command-line python tool to make the retrieval of testing results easier
for kernel maintainers.

Currently, a standard workflow for a kernel maintainer is to translate from
dist git --> brew --> retrieve nvr/taskID --> OSCI --> open tabs from CKIDB/OSCIartifacts.
It is also necessary to confirm you are investigating the correct build(s),
identify testcase names and investigate build logs, console logs, task logs and test logs.

igrab achieves the same workflow with the execution of `igrab -n $NVR --verbose`
which prints out the testcase names, URLs, and endpoint references
so that you always know what you're looking at, all the time.

## How does this relate to Datawarehouse?

igrab leverages the (currently *unstable*) GraphQL [endpoint],
which, in itself sources testing events for supported artifacts from the [UMB].
The GraphQL endpoint consumes test results across [Koji/Brew], [Dist-Git], [Greenwave], [WaiverDB]
& [ResultsDB], making it easy to gather a collection of data that is associated with a single NVR.

[igrab]: https://gitlab.com/debarbos/igrab
[endpoint]: https://dashboard.osci.redhat.com/graphql
[UMB]: https://datagrepper.engineering.redhat.com/umb/topics.html#
[Koji/Brew]: https://brewweb.engineering.redhat.com/
[Dist-Git]: https://pkgs.devel.redhat.com/
[Greenwave]: https://greenwave.engineering.redhat.com
[WaiverDB]: https://waiverdb.engineering.redhat.com
[ResultsDB]: https://resultsdb-api.engineering.redhat.com/api/v2.0/
