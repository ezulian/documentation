---
title: Rotating secrets
description: >
  How to systematically rotate all secrets
---

## Problem

You want to renew all secrets across the whole CKI project, e.g. when a project
member is leaving.

## Rotating master keys

### credentials repo

In the `credentials` repository, perform the following steps:

1. Check the git commits for the key IDs of the collaborators:

   ```shell
   git log
   ```

1. Make sure to import all their public GPG keys into your keyring. Those can
   be found on GitLab via `https://gitlab.com/username.gpg`.

1. Call `git-crypt-rm-gpg-user.sh` with the key ID to remove to re-encrypt the
   repository.

1. Check that the key actually got removed and remove it if necessary via

   ```shell
   git log
   git rebase -i origin/HEAD
   ```

1. Commit the changes via a new merge request.

### deployment-all

1. Create a new ENVPASSWORD and re-encrypt all secrets via

   ```shell
   export NEW_ENVPASSWORD=$(diceware)
   for key in $(shyaml keys < secrets.yml); do
     ENVPASSWORD=$NEW_ENVPASSWORD ./edit_secret.sh "$key" "$(cki_secret "$key")"
   done
   ```

1. Change the ENVPASSWORD variable in the [CI/CD settings][deployment-all-cicd]
   of [deployment-all] to the new value.

1. Commit the changes via a new merge request.

1. Document the new ENVPASSWORD in the `credentials` repository.

[deployment-all]: https://gitlab.cee.redhat.com/cki-project/deployment-all
[deployment-all-cicd]: https://gitlab.cee.redhat.com/cki-project/deployment-all/-/settings/ci_cd
