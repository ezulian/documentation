---
title: Triggering CKI tests for scratch builds
linkTitle: Triggering CKI scratch testing
description: >-
  How to trigger CKI pipelines for Koji/Brew scratch builds
weight: 60
---

## Goal

By default, scratch builds in Koji/Brew are not tested by CKI.

CKI testing for scratch builds can be triggered on a build-by-build basis by
including a special string in the `BUILDID`.

## Steps

1. Determine the test set that you want to invoke from the `sets` key in the
   [kpet-db][kpet-index] `index.yaml`. Alternatively, install [kpet] and get
   the available test sets via

   ```bash
   kpet set list
   ```

   By default, the same test set as for other Koji/Brew builds (`kt1`) is used.

2. Optionally, check that the expected tests are included in the test set
   `{SET}` for e.g. RHEL 8 by running

   ```bash
   kpet test list --tree rhel8 --sets {SET}
   ```

3. Submit the build to Koji/Brew for CKI testing. To use the default test set,
   include the string `.test.cki` in the `BUILDID` after the release. A
   specific test set `{SET}` can be tested by using `.test.cki.{SET}`.

   As an example, using the [Makefile] from kernel-ark derived repositories,
   the default CKI test set can be triggered via

   ```shell
    make BUILDID=".test.cki" dist-srpm
   ```

   You should receive emails about the progress of the testing pipeline once
   your Koji/Brew build completes.

[kpet-index]: https://gitlab.com/redhat/centos-stream/tests/kernel/kpet-db/-/blob/main/index.yaml
[kpet]: kpet.md
[Makefile]: https://gitlab.com/cki-project/kernel-ark/-/blob/os-build/redhat/Makefile
